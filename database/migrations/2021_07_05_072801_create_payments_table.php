<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cus_id');
            $table->string('invoice')->index();
            $table->string('card_number');
            $table->string('exp_date');
            $table->string('cvc');
            $table->string('payment_method');
            $table->decimal('amount');
            $table->timestamps();
            $table->foreign('cus_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('invoice')->references('invoice')->on('sales')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

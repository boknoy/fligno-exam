<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\SalesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/products', [ProductController::class, 'index']);

//Customer
Route::post('/customer/register',[CustomerController::class,'register']);
Route::post('/customer/login',[CustomerController::class,'login']);


//Cart
Route::get('/cart/{id}/all',[CartController::class,'getAllCart']);
Route::get('/cart/{id}',[CartController::class,'show']);
Route::put('/cart/{id}',[CartController::class,'update']);
Route::delete('/cart/{id}',[CartController::class,'delete']);
Route::post('/cart',[CartController::class,'store']);
//Order Temp
Route::post('/order',[OrderController::class,'store']);
Route::get('/order/{id}/{qty}/{c_id}/{cout}',[OrderController::class,'placeOrder']);
Route::get('/order/{id}',[OrderController::class,'show']);
//Checkout
Route::post('/payment',[CheckoutController::class,'store']);
Route::get('/invoice',[CheckoutController::class,'index']);
Route::get('/products/{id}', [ProductController::class, 'show']);
Route::delete('/cart/{id}', [CartController::class, 'destroy']);
Route::get('/customers', [CustomerController::class, 'getAllCustomer']);

Route::group(['middleware' => ['auth:sanctum']], function () {
   
    Route::post('/products', [ProductController::class, 'store']);
    Route::post('/update-products', [ProductController::class, 'updateProd']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);
    
    
    
    Route::get('/products/search/{name}', [ProductController::class, 'search']);
    //User
  
    Route::get('user/{id}/details', [AuthController::class, 'getAuthDetails']);
    Route::get('user/{id}/details2', [AuthController::class, 'getDetails']);
    Route::get('sales', [SalesController::class, 'index']);
    Route::post('/logout', [AuthController::class, 'logout']);

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
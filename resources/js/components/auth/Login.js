import React, { useState,useEffect } from 'react'
import {  useHistory ,Link} from 'react-router-dom'
import api from '../api'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
export default function Login()

{
    toast.configure()
    const history = useHistory()
    const [loading, setLoading] = useState(false)
    const [required, setRequired] = useState('form-control')
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    //const [user, setUser] = useState('');
  
     
    
    useEffect(() => {
      if(localStorage.getItem('userAuth')) {
        history.push('/home')
    }
  }, []);
    const loginUser = async () => {
        setLoading(true);
        try {
            await api.login({
                email, password
            }).then(response => {
                if(response.data.error) {
                    toast.error(response.data.error, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setRequired('form-control is-invalid')
                    setPassword('');
                }
                if(response.data.failed) {
                    toast.error(response.data.failed, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    
                  
                }
                if(response.data.success) {
                    localStorage.setItem('userAuth', response.data.token);
                   
                    history.push('/home');
                    toast.success(response.data.message, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
        } catch {
            console.log('ERROR CATCH')
        } finally {
            setLoading(false);
        }
    }
    $('#body').removeClass('dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed')
    $('#body').addClass('login-page')
    return(
        <div className="login-box mt-5">
        
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="#" className="h1"><b>Admin</b></a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Sign in to start your session</p>
      
            <form>
              <div className="input-group mb-3">
                <input type="email" className={required} placeholder="Email" value={email}  onChange={e => {setEmail(e.target.value);setRequired('form-control')}}
                                required/>
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input type="password" className={required} value={password} placeholder="Password"  onChange={e => {setPassword(e.target.value);setRequired('form-control')}}
                                required autoComplete="off"/>
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8">
                  <div className="icheck-primary">
                    <input type="checkbox" id="remember"/>
                    <label htmlFor="remember">
                      Remember Me
                    </label>
                  </div>
                </div>
              
                <div className="col-4">
                  <button type="submit" className="btn btn-primary btn-block" onClick={loginUser}
                                    disabled={loading}
                                >{loading ? 'Signing In...' : 'Login'}</button>
                </div>
             
              </div>
            </form>
      
            <div className="social-auth-links text-center mt-2 mb-3">
              <a href="#" className="btn btn-block btn-primary">
                <i className="fab fa-facebook mr-2"></i> Sign in using Facebook
              </a>
              <a href="#" className="btn btn-block btn-danger">
                <i className="fab fa-google-plus mr-2"></i> Sign in using Google+
              </a>
            </div>
           
      
            <p className="mb-1">
              <a href="forgot-password.html">I forgot my password</a>
            </p>
            <p className="mb-0">
              <a href="register.html" className="text-center">Register a new membership</a>
            </p>
          </div>
          
        </div>
       
      </div>
    )
}
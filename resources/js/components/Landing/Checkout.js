import React, {useState,useEffect, Component } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import api from '../api';
import Header from './Header'


export default function Checkout()

{
    toast.configure();
    const history = useHistory();
    const [cusID,setCusID] = useState('')
    const [customer,setCustomer] = useState('')
    const [details,setDetails] = useState([])
    const [mycart,setCart] = useState([])
    const [isLoaded,setLoaded] = useState(false)
    const [product,setProd] = useState([])
    const [amount,setAmount] = useState(0)
    const [total,setTotal] = useState(0)
    const [loading, setLoading] = useState(false)
    const [card_name,setCardName] = useState('')
    const [card_number,setCardNumber]=useState('5555555555554444')
    const [exp_month,setExpMonth] = useState('')
    const [exp_year,setExpYear] = useState('')
    const [cvc,setCvc] = useState('')
    useEffect(() => {
     
                api.getAuth().then(response => {
                  
                  setCustomer(response.data)
                    
                
                   api.getOrder(response.data.id).then ( res =>{
                    if(res.data.success)
                    {
                    
                      setTotal(res.data.total)
                      setCart(res.data.order)
                      setProd(res.data.cart.get_prod)
                      setDetails(res.data.det)
                      setLoaded(true)
                     
                    }
                  })
                });
              },[])
          
              console.log(mycart)
              const payment =async () => 
              {
                  setLoading(true)
                  const formdata = new FormData()
                  formdata.append('card_name',card_name)
                  formdata.append('card_number',card_number)
                  formdata.append('exp_month',exp_month)
                  formdata.append('exp_year',exp_year)
                  formdata.append('cvc',cvc)
                  formdata.append('cus_id',customer.id)
                  formdata.append('amount',total)
                  try {
                      await api.addPayment(formdata).then(response => {
                          if(response.data.success) {
                              setLoading(false)
                              toast.success(response.data.success, {
                                  position: "top-center",
                                  autoClose: 5000,
                                  hideProgressBar: true,
                                  closeOnClick: true,
                                  pauseOnHover: true,
                                  draggable: true,
                                  progress: undefined,
                              });
                              history.push('/invoice')
                          }
                          
                   
                      })
                  } catch(e) {
                      alert(e);
                  } finally {
                      setLoading(false);
                  }
              }
    const Order = () =>
    {
        if(isLoaded && mycart.length > 0)
        {
            return(
             mycart.map(row =>{
                return( <tr key={row.id}>
                  
                     <td></td>
                     <td>{row.get_prod.map(row2=> {return(<img key={row2.id} src={"lte3/dist/img/"+row2.prod_image} height="50px" width="50px" className="mr-2"></img>) })}{row.get_prod.map(row2=> {return(row2.prod_name) })} </td>
                     <td>₱ {row.get_prod.map(row2=> {return(row2.price) })}</td>
                    
                     <td>{row.quantity}</td>
                     
                     <td> ₱ {row.total /*amount ? (amount) : (row.quantity * row.get_prod.map(row2=> {return(row2.price) }) )*/}</td>
                    
                 </tr>)
               
             } ) )
               
             
        }
    }
        
    return(
        <div>
            <Header/>
            <div className="content-wrapper">
    
    <div className="content-header">
      <div className="container">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1 className="m-0"><Link className="material-icons mr-2" to="/cart">west</Link>Checkout </h1>
          </div>

        </div>
      </div>
    </div>
    <div className="content">
      <div className="container">
        <div className="row">
           
                <div className="col-sm-6">
                    <div className="card">
                        <div className="card-header">
                            <h3>Billing Information</h3>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                    <label>Name</label>
                                    <label className="form-control"  id="cus_name">{customer.name}</label>      
                                    </div>
                                    <div className="form-group">
                                    <label>Address</label>
                                    <label className="form-control"  id="address" >{details.address}</label>     
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                    <label>Email</label>
                                    <label className="form-control"  id="email">{customer.email}</label>      
                                    </div>
                                    <div className="form-group">
                                    <label>Contact</label>
                                    <label className="form-control"  id="contact">{details.contact}</label>    
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h3>Payment Method</h3>
                        </div>
                        <div className="card-body">
                           <div className="row">

                               <div className="col-sm-6">
                                   <div className="form-group">
                                       <label>Card holder's name</label>
                                       <input className="form-control" id="card_name"
                                       value={card_name}
                                       onChange={e=>setCardName(e.target.value)}></input>
                                   </div>
                                   <div className="form-group">
                                       <label>Expiry Month</label>
                                       <input className="form-control" id="month_exp" type="number"
                                       value={exp_month}
                                       onChange={e=>setExpMonth(e.target.value)}></input>
                                   </div>
                                   <div className="form-group">
                                       <label>CVC</label>
                                       <input className="form-control" id="month_exp" type="password"
                                       value={cvc}
                                       onChange={e=>setCvc(e.target.value)}></input>
                                   </div>
                               </div>
                               <div className="col-sm-6">
                                   <div className="form-group">
                                       <label>Card number</label>
                                       <input className="form-control" id="card_number" type="number"
                                       value={card_number} 
                                       onChange={e=>setCardNumber(e.target.value)}></input>
                                   </div>
                                   <div className="form-group">
                                       <label>Expiry Year</label>
                                       <input className="form-control" id="year_exp" type="number"
                                        value={exp_year} 
                                        onChange={e=> setExpYear(e.target.value)}>

                                        </input>
                                   </div>
                                   <div className="form-group">
                                       <label></label>
                                      
                                       <label className="col-sm-12">Grand Total : ₱ {total}</label>
                                       <button className="btn btn-success btn-md btn-block" onClick={payment} disabled={loading} >{loading ? 'Placing..' : 'Place Order'}</button>
                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="card">
                        <div className="card-header">
                            <h3>Order Summary</h3>
                        </div>
                        <div className="card-body">
                        <table className="table table-stripe ">
                            <thead>
                                <tr>
                               <th>
                                  
                               </th>
                               <th>
                                   Product
                               </th>
                               <th>
                                   Price
                               </th>
                               <th>
                                   Quantity
                               </th>
                               <th>
                                   Amount
                               </th>
                               </tr>
                            </thead>
                            <tbody>
                                {Order()}
                           
                               
                            </tbody>
                            <tfoot>
                                <tr>
                                <th colSpan={4} className="text-right">Total</th>
                                <th>₱ {total}</th>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
           
        </div>
    </div>
    </div>
    </div>
    
    </div>
    )
                      
}
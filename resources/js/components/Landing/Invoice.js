import React, {useState,useEffect, Component } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal,FormControl } from 'react-bootstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import api from '../api';
import Header from './Header'
import { findIndex, map, set } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';

export default function Invoice()


{
  //history
  const history = useHistory()
  //Loading
    const [loading,setLoading] = useState(false)
    const [isLoaded,setLoaded] = useState(false)

  //get
  const [invoice,setInvoice] = useState([])
  const [ordered,setOrdered] = useState([])
  const [customer,setCustomer] = useState([])
  const [details,setDetails] = useState([])
  const [total,setTotal] = useState(0)
  useEffect(() => {
    api.getInvoice().then (res =>{
        setInvoice(res.data.invoice)
        setOrdered(res.data.ordered)
        setTotal(res.data.total)
        setLoaded(true)
    })
    api.getAuth().then(res => {
        setCustomer(res.data)
        api.getDetails(res.data.id).then(res2 =>{
          setDetails(res2.data)
        })
    })
  },[])

  console.log(invoice)
  console.log(isLoaded)

  if(isLoaded && invoice.length >0)
  {
    return(
      <div>
          <Header/>
  <section className="content">
    <div className="container-fluid">
      <div className="row">
        <div className="col-12">
         


        
          <div className="invoice p-3 mb-3">
          
            <div className="row">
              <div className="col-12">
                <h4>
                  <i className="fas fa-globe"></i> Fligno, Inc.
                  <small className="float-right">{moment(ordered[0].created_at).format('MMMM/DD/YYYY')}</small>
                </h4>
              </div>
              
            </div>
     
            <div className="row invoice-info">
              <div className="col-sm-4 invoice-col">
                From
                <address>
                  <strong>Fligno, Inc.</strong><br/>
                  401 4F CKY Bldg, RN Abejuela St, Brgy 5<br/>
                  Cagayan de Oro, 9000<br/>
                  Phone: (088) 855 0110<br/>
                  Email: bagaresnilo@gmail.com
                </address>
              </div>
             
              <div className="col-sm-4 invoice-col">
                To
                <address>
                  <strong>{customer.name}</strong><br/>
                   {details.address}<br/>
                
                  Phone: {details.contact}<br/>
                  Email: {customer.email}
                </address>
              </div>
           
              <div className="col-sm-4 invoice-col">
                <b>Invoice #{invoice}</b><br/>
                <br/>
                <b>Order ID:</b> 4F3S8J<br/>
                <b>Payment Due:</b> 2/22/2014<br/>
                <b>Account:</b> 968-34567
              </div>
          
            </div>
       

            <div className="row">
              <div className="col-12 table-responsive">
                <table className="table table-striped">
                  <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                   
                  </tr>
                  </thead>
                  <tbody>
                    {ordered.map(row=>{return(
                        <tr key={row.id}>
                          <td>{row.quantity}</td>
                          <td>{row.get_prod.map(row2=> {return(<img key={row2.id} src={"lte3/dist/img/"+row2.prod_image} height="50px" width="50px" className="mr-2"></img>) })}{row.get_prod.map(row2=> {return(row2.prod_name) })} </td>
                          <td>₱ {row.get_prod.map(row2=> {return(row2.price) })}</td>
                          <td>₱{row.total}</td>
                        </tr>


                    )})}
                     

                  </tbody>
                  <tfoot>
                      <tr>
                          <th></th>
                      </tr>
                  </tfoot>
                </table>
              </div>
           
            </div>
         

            <div className="row">
           
              <div className="col-6">
                <p className="lead">Payment Methods:</p>
                <img src="lte3/dist/img/credit/visa.png" alt="Visa"/>
                <img src="lte3/dist/img/credit/mastercard.png" alt="Mastercard"/>
                <img src="lte3/dist/img/credit/american-express.png" alt="American Express"/>
                <img src="lte3/dist/img/credit/paypal2.png" alt="Paypal"/>

                <p className="text-muted well well-sm shadow-none">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                  plugg
                  dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
              </div>
              
              <div className="col-6">
                <p className="lead">Amount Due 2/22/2014</p>

                <div className="table-responsive">
                <table className="table">
                  <thead>

                  </thead>
                  <tbody>
                  <tr>
                        <th style={{"width": "50%"}}><b>Subtotal:</b></th>
                        <td>₱ {total - (total * .12 )}</td>
                      </tr>
                      <tr>
                        <th>Tax (12%)</th>
                        <td>₱ {(total * .12).toFixed(2)}</td>
                      </tr>
                      
                      <tr>
                        <th>Total:</th>
                        <td>₱ {total.toLocaleString(undefined, {maximumFractionDigits:2})}</td>
                      </tr>
                  </tbody>
                      
                    </table>
                </div>
              </div>
    
            </div>
     

          
            <div className="row no-print">
              <div className="col-12">
                <a href="invoice-print.html" rel="noopener" target="_blank" className="btn btn-default"><i className="fas fa-print"></i> Print</a>
                <button type="button" className="btn btn-success float-right"><i className="far fa-credit-card"></i> Submit
                  Payment
                </button>
                <button type="button" className="btn btn-primary float-right" >
                  <i className="fas fa-download"></i> Generate PDF
                </button>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </section>
      </div>
  )
  }else if(isLoaded && invoice.length == 0)
  {
    history.push('/')
    return (<div></div>)
  }else

  {
    return (<h1>Loading...</h1>)
  }
}
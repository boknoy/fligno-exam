import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import api from '../api';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
export default function Header()

{
  toast.configure();
    const history = useHistory();
    const [loading, setLoading] = useState(false)



    //modal
    const [logoutModal, setLogoutModal] = useState(false);
    const [regModal,setRegModal] = useState(false)
    const [loginModal,setLoginModal] = useState(false)
    const modalOpen = () => setRegModal(true)
    const modalClose =() => setRegModal(false)
    const logoutModalOpen = () => setLogoutModal(true);
    const logoutModalClose = () => setLogoutModal(false);
    const loginModalOpen = () => setLoginModal(true);
    const loginModalClose = () => setLoginModal(false);
    const [guest,setGuest] = useState(true)
    //Customer 
    const [name,setName] = useState('')
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [address,setAdd] = useState('')
    const [contact,setContact] = useState('')
    const [customer,setCustomer] = useState([])
    const [details,setDetails] = useState([])

      useEffect(() => {
        if(!localStorage.getItem('userAuth')) {
          setGuest(true)
         
      }else
      {
        api.getAuth().then(res => {
          setCustomer(res.data)
          setGuest(false)
      })
      }
       
    }, []);

   
    const register =async () => 
    {
      setLoading(true)
        const formdata = new FormData()
        formdata.append('name',name)
        formdata.append('email',email)
        formdata.append('password',password)
        formdata.append('address',address)
        formdata.append('contact',contact)
        try {
            await api.registerCustomer(formdata).then(response => {
           
                if(response.data.success) {
                  localStorage.setItem('userAuth', response.data.token);
                  toast.success(response.data.success, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setGuest(false)
                  
                }
                if(response.data.message) {
                  toast.error(response.data.message, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                }
         
            })
        } catch(e) {
            alert(e);
        } finally {
            setRegModal(false)
            setLoading(false);
        }
    }
    const login =async () => 
    {
      setLoading(true)
        const formdata = new FormData()
        formdata.append('name',name)
        formdata.append('email',email)
        formdata.append('password',password)
    
        try {
            await api.loginCustomer(formdata).then(response => {
           
                if(response.data.success) {
                  
                  localStorage.setItem('userAuth', response.data.token);
                  toast.success(response.data.success, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setGuest(false)
                    
                }
                if(response.data.failed) {
                  toast.error(response.data.failed, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                setPassword('')
                }
                
            })
        } catch(e) {
            alert(e);
        } finally {
          setLoginModal(false)
            setLoading(false);
        }
    }
    const logoutUser = async () => {
      setLoading(true)
      try {
          await api.logout().then(response => {
              localStorage.clear();
              location.reload()
              toast.info(response.data.message, {
                  position: "top-center",
                  autoClose: 5000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
              });
          })
      } catch {
          console.log(error)
      } finally {
          setLogoutModal(false)
          setLoading(false);
          setGuest(true)
      }
  }
  $('#body').removeClass('dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed login-page')
    $('#body').addClass('layout-top-nav')
    return(
        <nav className="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div className="container">
            <Link to="/" className="navbar-brand">
                <img src="lte3/dist/img/fligno.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" opacity={0.8} />
                <span className="brand-text font-weight-light">Fligno</span>
            </Link>

            <button className="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

      <div className="collapse navbar-collapse order-3" id="navbarCollapse">
       
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link to="/" className="nav-link">Products</Link>
          </li>
         
         
     {guest ? (<> <li className="nav-item">
          <a className="nav-link"  onClick={loginModalOpen} role="button" >
            <i className="fa fa-"> Login</i>
          </a>   </li> <li className="nav-item"> <a className="nav-link"  onClick={modalOpen} role="button">
            <i className="fa fa"> Sign up</i>
          </a></li></>):
          ( <><li className="nav-item"><a className="nav-link"  role="button">
          <i className="fa fa-user"> Hi! { customer.name}</i>
        </a></li> <li className="nav-item"><a className="nav-link"  role="button" onClick={logoutModalOpen}>
          <i className="fa  fa-sign-out-alt">Logout</i>
        </a></li></>)}

     
         
        
        </ul>

        
       
      </div>

      
      <ul className="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
       
     
       
       
      {!guest &&  <li className="nav-item">
          <Link className="nav-link"  to="/cart" role="button">
            <i className="fa fa-cart-plus"></i>
          </Link>
        </li>}
       
     
      </ul>
    </div>
    <Modal size="md" show={regModal} onHide={modalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>REGISTER CUSTOMER</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                              <div className="col-sm-6">
                                <div className="form-group">
                                  <label>Full Name</label>
                                  <input className="form-control form-control-sm" value={name} onChange={e=>setName(e.target.value)} ></input>
                                </div>
                                <div className="form-group">
                                  <label>Address</label>
                                  <input className="form-control form-control-sm" value={address} onChange={e=>setAdd(e.target.value)} ></input>
                                </div>
                              </div>
                              <div className="col-sm-6">
                                <div className="form-group">
                                  <label>Email</label>
                                  <input className="form-control form-control-sm" value={email} onChange={e=>setEmail(e.target.value)} ></input>
                                </div>
                                <div className="form-group">
                                  <label>Contact</label>
                                  <input className="form-control form-control-sm" value={contact} onChange={e=>setContact(e.target.value)} ></input>
                                </div>
                              </div>
                             
                              <div className="col-sm-6">
                                <div className="form-group">
                                  <label>Password</label>
                                  <input type="password" autoComplete="off" className="form-control form-control-sm" value={password} onChange={e=>setPassword(e.target.value)} placeholder="********"></input>
                                </div>
                                <div className="form-group">
                                  <label>Re-password</label>
                                  <label className="form-control form-control-sm"  >********</label>
                                </div>
                              </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={modalClose}>
                            Cancel
                        </Button>
                        
                            <Button variant="primary" disabled={loading} onClick={register} >
                            {loading ? 'Register...' : 'Register'}
                        </Button>
                     
                      
                    </Modal.Footer>
                </Modal> 
                <Modal size="md" show={loginModal} onHide={loginModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>LOGIN CUSTOMER</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                              <div className="col-sm-12">
                               
                                <div className="form-group">
                                  <label>Email</label>
                                  <input className="form-control form-control-sm" value={email} onChange={e=>setEmail(e.target.value)} ></input>
                                </div>
                                <div className="form-group">
                                  <label>Password</label>
                                  <input type="password" className="form-control form-control-sm" value={password} onChange={e=>setPassword(e.target.value)} placeholder="*******" autoComplete="false"></input>
                                </div>
                             
                              </div>
                             
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={loginModalClose}>
                            Cancel
                        </Button>
                        
                            <Button variant="primary" disabled={loading} onClick={login} >
                            {loading ? 'Loginn...' : 'Login'}
                        </Button>
                     
                      
                    </Modal.Footer>
                </Modal> 
                <Modal size="md" show={logoutModal} onHide={logoutModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Logout</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Are you sure you want to logout?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={logoutModalClose}>
                            Close
                        </Button>
                        <Button variant="primary" disabled={loading} onClick={logoutUser}>
                            {loading ? 'Loading...' : 'Confirm'}
                        </Button>
                    </Modal.Footer>
                </Modal> 
  </nav>
 

  
  
    )
}
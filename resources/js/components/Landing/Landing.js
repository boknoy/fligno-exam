import React, {useState,useEffect, Component } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import api from '../api';
import Header from './Header'

export default function Landing()

 
{
  toast.configure();
  const history = useHistory();
  const [products, setProduct] = useState('')
  const [isLoaded,setLoaded] = useState(false)
  const [prodModal,setProdModal] = useState(false)
  const cartModalOpen = () => setProdModal(true)
  const cartModalClose =() => setProdModal(false)
  const [loading, setLoading] = useState(false)
  //Customer 
  const [cusID,setCusID] = useState('')
  const [customer,setCustomer] = useState('')
//Prod
  const [prodName,setProdName] = useState('')
  const [prodImage,setProdImage] = useState('')
  const [quantity,setQuantity] = useState(1)
  const [prodId,setProdID] = useState()
  const [guest,setGuest] = useState(true)
  useEffect(() => {
    api.getAllProduct().then(res => {
                setProduct(res.data)
                setLoaded(true)
            })
            if(!localStorage.getItem('userAuth'))
            {
             setGuest(false)
            }
            else
            {
              api.getAuth().then(response => {
                setCusID(response.data.id)
                setCustomer(response.data)
                api.getAuthDetails(response.data.id).then(res => {
                    
    
                });
               
            })
            }
            
        },[]);
    const getProduct = (prod_id) => 
    {
      api.getProd(prod_id).then(res => {

          setProdName(res.data.prod_name)
          setProdImage("lte3/dist/img/"+res.data.prod_image)
          setProdID(res.data.id)
          cartModalOpen()
      })
    } 
    
    const addCart = async () => 
    {
      const formdata = new FormData()
      formdata.append('prod_id',prodId)
      formdata.append('quantity',quantity)
      formdata.append('cus_id',cusID)
     await api.addtoCart(formdata).then( res => {
        cartModalClose()
          if(res.data.success)
          {
            toast.success(res.data.success, {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
          });
          setQuantity(1)
          }
          if(res.data.message)
          {
            
            toast.info(res.data.message, {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
          });
          }

      })
    }

    $('#body').removeClass('dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed login-page')
    $('#body').addClass('layout-top-nav')

        return(

            <div>
                <Header/>
                <div className="content-wrapper">
    
          
    <div className="content ">
      <div className="container">
        <div className="row mb-5">
        {isLoaded &&  products.map (row =>{
                                            return(
                                           

        <div className="card card-solid" key={row.id}>
        <div className="card-body">
          <div className="row">
            <div className="col-12 col-sm-6">
              <h3 className="d-inline-block d-sm-none">{row.prod_name}</h3>
              <div className="col-12">
                <img src={"lte3/dist/img/"+row.prod_image} className="product-image" alt="Product Image"/>
              </div>
              <div className="col-12 product-image-thumbs">
                <div className="product-image-thumb active"><img src="lte3/dist/img/prod-1.jpg" alt="Product Image"/></div>
                <div className="product-image-thumb" ><img src="lte3/dist/img/prod-2.jpg" alt="Product Image"/></div>
                <div className="product-image-thumb" ><img src="lte3/dist/img/prod-3.jpg" alt="Product Image"/></div>
                <div className="product-image-thumb" ><img src="lte3/dist/img/prod-4.jpg" alt="Product Image"/></div>
                <div className="product-image-thumb" ><img src="lte3/dist/img/prod-5.jpg" alt="Product Image"/></div>
              </div>
            </div>
            <div className="col-12 col-sm-6">
              <h3 className="my-3">{row.prod_name}</h3>
              <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p>

              <hr/>
              <h4>Available Colors</h4>
              <div className="btn-group btn-group-toggle" data-toggle="buttons">
                <label className="btn btn-default text-center active">
                  <input type="radio" name="color_option" id="color_option_a1" autoComplete="off" />
                  Green
                <br/>
                  <i className="fas fa-circle fa-2x text-green"></i>
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_a2" autoComplete="off"/>
                  Blue
                <br/>
                  <i className="fas fa-circle fa-2x text-blue"></i>
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_a3" autoComplete="off"/>
                  Purple
                <br/>
                  <i className="fas fa-circle fa-2x text-purple"></i>
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_a4" autoComplete="off"/>
                  Red
                <br/>
                  <i className="fas fa-circle fa-2x text-red"></i>
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_a5" autoComplete="off"/>
                  Orange
                <br/>
                  <i className="fas fa-circle fa-2x text-orange"></i>
                </label>
              </div>

              <h4 className="mt-3">Size <small>Please select one</small></h4>
              <div className="btn-group btn-group-toggle" data-toggle="buttons">
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_b1" autoComplete="off"/>
                  <span className="text-xl">S</span>
                <br/>
                  Small
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_b2" autoComplete="off"/>
                  <span className="text-xl">M</span>
                <br/>
                  Medium
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_b3" autoComplete="off"/>
                  <span className="text-xl">L</span>
                <br/>
                  Large
                </label>
                <label className="btn btn-default text-center">
                  <input type="radio" name="color_option" id="color_option_b4" autoComplete="off"/>
                  <span className="text-xl">XL</span>
                <br/>
                  Xtra-Large
                </label>
              </div>

              <div className="bg-gray py-2 px-3 mt-4">
                <h2 className="mb-0"> ₱
                 {row.price}
                </h2>
                <h4 className="mt-0">
                  <small>Ex Tax: 12 % </small>
                </h4>
              </div>

              <div className="mt-4">
                <div className="btn btn-primary btn-lg btn-flat" onClick={()=>getProduct(row.id)}>
                  <i className="fas fa-cart-plus fa-lg mr-2"></i>
                  Add to Cart
                </div>

                <div className="btn btn-default btn-lg btn-flat">
                  <i className="fas fa-heart fa-lg mr-2"></i>
                  Add to Wishlist
                </div>
              </div>

              <div className="mt-4 product-share">
                <a href="#" className="text-gray">
                  <i className="fab fa-facebook-square fa-2x"></i>
                </a>
                <a href="#" className="text-gray">
                  <i className="fab fa-twitter-square fa-2x"></i>
                </a>
                <a href="#" className="text-gray">
                  <i className="fas fa-envelope-square fa-2x"></i>
                </a>
                <a href="#" className="text-gray">
                  <i className="fas fa-rss-square fa-2x"></i>
                </a>
              </div>

            </div>
          </div>
        
        </div>
        
      </div>
         
         ) }
         ) 
     }
        </div>
        
      </div>
      
    </div>
    </div>
    <Modal size="md" show={prodModal} onHide={cartModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{prodName}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                          <div className="form-group col-sm-6">
                              <img height="100px" width="auto" src={prodImage}   />
                          </div>
                          <div className="form-group col-sm-6">
                            <label>Enter Quantity</label>
                            <input type="number"  className="form-control" id="quantity" value={quantity} onChange={e=>setQuantity(e.target.value)} />
                          </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={cartModalClose}>
                            Cancel
                        </Button>
                        
                            <Button variant="primary" disabled={loading} onClick={addCart} >
                            {loading ? 'Ssaving...' : 'Add to cart'}
                        </Button>
                     
                      <input value={prodId} type="hidden"/>  
                    </Modal.Footer>
                </Modal> 
    </div>
    
        )
  
}
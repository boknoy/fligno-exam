import React, {useState,useEffect, Component } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal,FormControl } from 'react-bootstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import api from '../api';
import Header from './Header'
import { findIndex, map, set } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
export default function Cart()

{
    const history = useHistory()
    const [cusID,setCusID] = useState('')
    const [customer,setCustomer] = useState('')
    const [mycart,setCart] = useState([])
    const [isLoaded,setLoaded] = useState(false)
    const [isChecout,setCheckout] = useState(false)
    const [loading,setLoading] = useState(false)
    const [product,setProd] = useState([])
    const [amount,setAmount] = useState(0)
    const [total,setTotal] = useState()
    const [quantity,setQuantity] = useState([])
    const [Stotal,setSub] = useState([])

    const [all,setAll] = useState(false)
    const [chck,setChck] = useState([])
    const [btnChck,setBtnChck] = useState(false)
    useEffect(() => {
     
                api.getAuth().then(response => {
                  
                  setCustomer(response.data)
                    
                
                   api.getCusCart(response.data.id).then (res =>{
                    if(res.data.success)
                    {
                      setTotal(res.data.total)
                      setCart(res.data.cart)
                      
                
                  
                      res.data.cart.map((e,i)=>{
                       
                        quantity[i]=e.quantity 
                      
                    })
                      setQuantity(quantity)
                      setProd(res.data.cart.get_prod)
                      setLoaded(true)
                      if(res.data.cart.length == 0)
                      {
                          setBtnChck(false)
                      }
                      else
                      {
                       
                        setBtnChck(true)

                      }
                     
                    }
                  })
                });
              },[])
         console.log(quantity) 
         console.log(mycart)
      const  change = (val,i,price) =>
      {
        if(val == 0)
        {
          val=1
          $('#quan'+i).val(1)
          return false
          
        }
       let stotal = val * price
        quantity[i] = val
 
        $('#stotal'+i).val(stotal)
       
         
        
        let sub = document.getElementsByName('stotal[]').length
        let sub2 = 0
        for(let i = 0;i<sub;i++)
        {
          sub2 = sub2 + parseInt($('#stotal'+i).val())
        }
       setTotal(sub2)
      }  
      
      const handleCheck = () =>
      {
        let all = document.getElementsByName('chck[]').length
        let chck = document.getElementsByName('chck[]');
        console.log("chck" +all)
          if($('#all').is(':checked'))
          {
            for(let i = 0;i<all;i++)
            {
              chck.item(i).checked=true
            }
          }else
          {
            for(let i = 0;i<all;i++)
            {
              chck.item(i).checked=false
            }
          }
      }

      const handleOrder = async () =>
      {
        
        setLoading(true)
        let checked =[]
        $.each($("input[name='chck[]']:checked"), function(){
          checked.push($(this).val());
      });
      if(checked.length == 0)
      {
        alert('No ordered yet')
        setLoading(false)
        return false
      }
        let qty =[]
       checked.map((v,i)=>{
         qty[i] = $('#quan'+v).val()
       })
      console.log(checked)
      console.log(qty)
        const formdata = new FormData()
        formdata.append('cart_id',checked)
        formdata.append('quantity',qty)
        formdata.append('cus_id',customer.id)
        let cout = checked.length
        try {
          await api.order(formdata).then(res => {
              setLoading(false)
                if(res.data.success)
                {
                    alert('ok')
                    history.push('/checkout')
                    
                }

       
          })
      } catch(e) {
          alert(e);
      } finally {
         
      }
      }
   
       const renderCart = () =>
       {
         
         if(isLoaded && mycart.length == 0)
         {
          
           return (<div className="alert alert-info"><h5><i className="icon fas fa-info"></i> Ooops YOUR CART IS EMPTY</h5></div>)
           
         }
         else if(!isLoaded)
         {
          return (<div className="alert alert-info"><h5><i className="icon fas fa-info"></i> Loading ...</h5></div>)
         }
         
         else{
           
            return(
              
            <table className="table table-stripe table-responsive">
              
            <thead>
                <tr>
               <th>
               <div className="icheck-primary d-inline">
                  <input type="checkbox"  id="all"  defaultChecked={all} onChange={handleCheck}/>
                  <label htmlFor="all">
                  </label>
                </div>
               </th>
               <th>
                   Product
               </th>
               <th>
                   Price
               </th>
               <th>
                   Quantity
               </th>
               <th>
                   Amount
               </th>
               </tr>
            </thead>
            <tbody>
            {isLoaded && mycart.map((row,index) =>{
               return( <tr key={row.id}>
                 
                    <td>  <div className="icheck-primary d-inline">
                  <input type="checkbox" id={"chck"+index} name="chck[]" defaultChecked={false} value={row.id}/>
                  <label htmlFor={"chck"+index}>
                  </label>
                </div></td>
                  
                    <td>{row.get_prod.map(row2=> {return(<img key={row2.id} src={"lte3/dist/img/"+row2.prod_image} height="50px" width="50px" className="mr-2"></img>) })}{row.get_prod.map(row2=> {return(row2.prod_name) })} </td>
                    <td>₱ {row.get_prod.map(row2=> {return(row2.price) })}</td>
                       
                    <td><FormControl id={"quan"+row.id} min={1}  key={row.id} className="form-control qty" type="number" width={10}  name="quntity[]"  defaultValue={row.quantity} onChange={e=>change(e.target.value,index,row.get_prod.map(row2=> {return(row2.price) }))} /></td>
                    
                    <td> ₱ <input className="Nob" defaultValue={row.total} readOnly id={"stotal"+index} name="stotal[]"></input></td>
                   
                </tr>)
              
            } ) }
               
            </tbody>
            <tfoot>
                <tr>
                <th colSpan={4} className="text-right">Total</th>
                <th>₱ {total}</th>
                </tr>
            </tfoot>
        </table>)
         }
       }
    return(
        <div>
          
            <Header/>
            <div className="content-wrapper">
    
    <div className="content-header">
      <div className="container">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1 className="m-0"> My Cart </h1>
          </div>
         
        </div>
      </div>
    </div>
    <div className="content">
      <div className="container">
      
            <div className=" col-sm-12 justify-content-center d-flex ">
                <div className="card col-sm-10">
                 
                    <div className="card-body">
                          {renderCart()}
                        
                        <div className="form-group col-sm-12">
                             {btnChck && <button className="btn btn-primary btn-sm float-right" onClick={handleOrder} disabled={loading}> {loading? 'Checkout...': 'Checkout'}</button> } 
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
    </div>
    
    </div>
    )
                      
}
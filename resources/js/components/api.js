const axios = window.axios;
const BASE_API_URL = 'http://127.0.0.1:8000/api';

export default
{
    login: (user) =>
    axios.post(`${BASE_API_URL}/login`, user),
    getAuth: () =>
    axios.get(`${BASE_API_URL}/user`, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    loginCustomer: (user) =>
    axios.post(`${BASE_API_URL}/customer/login`, user),
    getAuth: () =>
    axios.get(`${BASE_API_URL}/user`, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    getAuthDetails: (id) =>
    axios.get(`${BASE_API_URL}/user/${id}/details`, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    getSales : () =>
    axios.get(`${BASE_API_URL}/sales`, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    getDetails: (id) =>
    axios.get(`${BASE_API_URL}/user/${id}/details2`, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    getAllProduct : () =>
    axios.get(`${BASE_API_URL}/products`,{
        headers: {
            Accept: 'application/json',
           // Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    logout: (user) =>
    axios.post(`${BASE_API_URL}/logout`, user, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    getAllUsers: () =>
    axios.get(`${BASE_API_URL}/users`),
   
    addProduct: (data)=>
    axios.post(`${BASE_API_URL}/products`,data,{

        headers: {
            'Content-Type': 'multipart/form-data',
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    deleteProd: (id)=>
    axios.delete(`${BASE_API_URL}/products/${id}`,{

        headers: {
            
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    updateProd: (data)=>
    axios.post(`${BASE_API_URL}/update-products`,data,{

        headers: {
            'Content-Type': 'multipart/form-data',
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    addPayment : (data) =>
    axios.post(`${BASE_API_URL}/payment`,data,{

        headers: {
            'Content-Type': 'multipart/form-data',
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    addtoCart: (data) =>
    axios.post(`${BASE_API_URL}/cart`,data,{

        headers: {
            'Content-Type': 'multipart/form-data',
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    order: (data) =>
    axios.post(`${BASE_API_URL}/order`,data,{

        headers: {
            'Content-Type': 'multipart/form-data',
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    
    registerCustomer : (data) => 
    axios.post(`${BASE_API_URL}/customer/register`,data,{
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    }),
    getCusCart: (id) =>
    axios.get(`${BASE_API_URL}/cart/${id}/all`,{
        Accept: 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('userAuth')
    }),

    getOrder: (id) =>
    axios.get(`${BASE_API_URL}/order/${id}/`,{
        Accept: 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('userAuth')
    }),
    getInvoice: () =>
    axios.get(`${BASE_API_URL}/invoice`,{
        Accept: 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('userAuth')
    }),
    getProd: (id)=>
    axios.get(`${BASE_API_URL}/products/${id}`,{

        headers: {
          
            Accept: 'application/json',
           // Authorization: 'Bearer ' + localStorage.getItem('userAuth')
        }
    }),
    getAllCustomer: () =>
            axios.get(`${BASE_API_URL}/customers`,{
                headers: {
                    Accept: 'application/json',
                    Authorization: 'Bearer ' + localStorage.getItem('userAuth')
                }
            })
}
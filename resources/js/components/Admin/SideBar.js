import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import api from '../api';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

export default function SideBar()

{
    toast.configure();
    const history = useHistory();
    const [loading, setLoading] = useState(false)
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [logoutModal, setLogoutModal] = useState(false);

    if(!localStorage.getItem('userAuth')) {
        history.push('/');
        toast.error('Unauthorized! ', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }

    const logoutModalOpen = () => setLogoutModal(true);
    const logoutModalClose = () => setLogoutModal(false);

    useEffect(() => {
        api.getAuth().then(response => {
            setId(response.data.id)
            
            api.getAuthDetails(response.data.id).then(res => {
                setName(res.data.name)
                if(res.data.role != 'Admin')
                {
                 // history.push('/')
                  toast.error('Unauthorized! ', {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                }
            });
           
        });
    }, []);


    const logoutUser = async () => {
        setLoading(true)
        try {
            await api.logout().then(response => {
                localStorage.clear();
                history.push('/');
                toast.info(response.data.message, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            })
        } catch {
            console.log(error)
        } finally {
            setLoading(false);
        }
    }
    return(
      <>
<aside className="main-sidebar sidebar-dark-primary elevation-4">
   
    <Link to="/" className="brand-link">
      <img src="lte3/dist/img/fligno.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" opacity={.8}/>
      <span className="brand-text font-weight-light">Admin</span>
    </Link>

    
    <div className="sidebar">
    
      <div className="user-panel mt-3 pb-3 mb-3 d-flex">
        <div className="image">
          <img src="lte3/dist/img/user2-160x160.jpg" className="img-circle elevation-2" alt="User Image"/>
        </div>
        <div className="info">
          <a href="#" className="d-block">{name}</a>
        </div>
      </div>

     
      <div className="form-inline">
        <div className="input-group" data-widget="sidebar-search">
          <input className="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
          <div className="input-group-append">
            <button className="btn btn-sidebar">
              <i className="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      
      <nav className="mt-2">
        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
          <li className="nav-item">
            <Link to="/home" className="nav-link active">
              <i className="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i className="right fas fa-angle-left"></i>
              </p>
            </Link>
          
          </li>
          <li className="nav-item ">
            <Link to="/sales" className="nav-link ">
              <i className="nav-icon fas fa-shopping-cart"></i>
              <p>
                Sales
                
              </p>
            </Link>
          
          </li>
         <li className="nav-item ">
            <Link to="/products" className="nav-link ">
              <i className="nav-icon fas fa-box"></i>
              <p>
                Manage Products
                
              </p>
            </Link>
          
          </li>
       
          

          <li className="nav-item ">
            <a href="#" className="nav-link " onClick={logoutModalOpen}>
              <i className="nav-icon  fa fa-sign-out-alt"></i>
              <p>
               Logout
                
              </p>
            </a>
          
          </li>
          </ul>
      </nav>
  
    </div>
    <Modal size="md" show={logoutModal} onHide={logoutModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Logout</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Are you sure you want to logout?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={logoutModalClose}>
                            Close
                        </Button>
                        <Button variant="primary" disabled={loading} onClick={logoutUser}>
                            {loading ? 'Loading...' : 'Confirm'}
                        </Button>
                    </Modal.Footer>
                </Modal> 
  </aside>
  </>
    );
}
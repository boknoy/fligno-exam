import React,{useEffect,useState} from 'react'
import { Link, useHistory } from 'react-router-dom';
import api from '../api';
import SideBar from './SideBar'
import Header from "./Header"

export default function DashBoard()
{
  const history  = useHistory()
  const [customer,setCustomer] = useState('')
  const [isLoaded,setLoaded] = useState('')
  const [customerCount,setCount] = useState('')
  const [sales,setSales] = useState(0)
  useEffect(() => {
    api.getAllCustomer().then(res => {
      setCustomer(res.data.customer)
      setLoaded(true)
      setCount(res.data.customerCount)
      setSales(res.data.sales)
  })
}, []);



    return(
      
        <div>
          <Header/>
      <SideBar/>
           <div className="content-wrapper">
    
    <div className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1 className="m-0">Dashboard</h1>
          </div>
         
        </div>
      </div>
    </div>
   

    
    <section className="content">
      <div className="container-fluid">
       
        <div className="row">
          
         
         
         

          
          <div className="clearfix hidden-md-up"></div>

          <div className="col-12 col-sm-6 col-md-3">
            <div className="info-box mb-3">
              <span className="info-box-icon bg-success elevation-1"><i className="fas fa-shopping-cart"></i></span>

              <div className="info-box-content">
                <span className="info-box-text">Sales</span>
                <span className="info-box-number">₱ {sales.toLocaleString(undefined, {maximumFractionDigits:2})}</span>
              </div>
          
            </div>
            
          </div>
        
          <div className="col-12 col-sm-6 col-md-3">
            <div className="info-box mb-3">
              <span className="info-box-icon bg-warning elevation-1"><i className="fas fa-users"></i></span>

              <div className="info-box-content">
                <span className="info-box-text"> Customers</span>
                <span className="info-box-number">{customerCount}</span>
              </div>
              
            </div>
           
          </div>
          
        </div>
       



  
    
    </div>
    </section>
    </div>
</div>
    )

}
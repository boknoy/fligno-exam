import React, {useState,useEffect, Component } from 'react';
import api from '../api';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import Header from './Header'
import Sidebar from './SideBar'
export default function Product ()
{
    
    
    
      const history = useHistory();
      const [image,setImage] = useState('')
      const [id,setProdId] = useState('')
      const [filename, setFilename] = useState('');
      const [prod_name , setProdname] = useState('')
      const [quantity , setQuantity] = useState('')
      const [price,setPrice] = useState('')


      const[modalTitle,setModalTitle] =useState('Add Product')
      const [products, setProduct] = useState('')
      const [isLoaded,setLoaded] = useState(false)
      const [isUpdate,setUpdate] = useState(false)
      const[newdata,setNewData] = useState('')
      const [prodModal,setProdModal] = useState(false)
      const [loading, setLoading] = useState(false)
      const [btnUpdate,setBtnupdate] = useState(false)
        useEffect(() => {
        api.getAllProduct().then(res => {
                    setProduct(res.data)
                    setLoaded(true)
                    
                })
            },[]);

            const prodModalOpen = () => setProdModal(true)
            const prodModalClose =() => setProdModal(false)
            const showAddModal = async () => 
            {
                setProdModal(true)
                setModalTitle('Add Product')
                setProdname('')
                    setPrice('')
                    setQuantity('')
                    setFilename('') 
                    setBtnupdate(false)  
            }
            const fileInput = (e) => {
                setImage(e.target.files[0]);
                setFilename(e.target.files[0].name)
            }
           const deleteProduct = (prod_id) =>
                {
             
                     api.deleteProd(prod_id).then(res => {
                        
                         $('#tr'+prod_id).remove()
                        if(res.data.success) {
                            toast.success(res.data.success, {
                                position: "top-center",
                                autoClose: 5000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        }
                        
                    })
               
            }
            const getProduct = (prod_id) =>
                {
                    
             
                 api.getProd(prod_id).then(res => {
                    setProdModal(true)
                    setModalTitle('Update Product')
                    setProdId(res.data.id)
                    setProdname(res.data.prod_name)
                    setPrice(res.data.price)
                    setQuantity(res.data.quantity)
                    setFilename(res.data.prod_image)
                    setBtnupdate(true) 
                    })
               
            }
            const updateProduct = () =>
            {
                const formdata = new FormData()
                formdata.append('prod_name',prod_name)
                formdata.append('prod_image',filename)
                formdata.append('price',price)
                formdata.append('quantity',quantity)
                formdata.append('id',id)
                api.updateProd(formdata).then(res => {
                    setProdModal(false)
                    $('#tr' + res.data.product.id + ' td:nth-child(1)' ).text("").append('<img src="lte3/dist/img/'+res.data.product.prod_image+'"  hieght="50px" width="50px"/>'+res.data.product.prod_name)
                    $('#tr' + res.data.product.id + ' td:nth-child(2)' ).text(res.data.product.quantity)
                    $('#tr' + res.data.product.id + ' td:nth-child(3)' ).text(res.data.product.price)
                    $('#tr' + res.data.product.id + ' td:nth-child(4)' ).text(res.data.product.quantity)
                   
                        $('#tr' + res.data.product.id + ' td:nth-child(4)' ).text('').append('<div><button class="btn btn-info btn-sm" onClick="getProducts('+res.data.product.id+')" ><i class="fas fa-edit"> Edit</i></button><button class="btn btn-danger btn-sm ml-1"><i className="fas fa-times-circle"> Delete</i> </button></div>')
                    
                   
                    

                    toast.success(res.data.success, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });  

                })
            }
         
            const addproducts =async () => 
            {
                const formdata = new FormData()
                formdata.append('prod_name',prod_name)
                formdata.append('prod_image',filename)
                formdata.append('price',price)
                formdata.append('quantity',quantity)
                try {
                    await api.addProduct(formdata).then(response => {
                        if(response.data.success) {
                        prodModalClose()
                        setLoaded(true)
                        setNewData(response.data.product)
                        setUpdate(true)
                       
                            toast.success(response.data.success, {
                                position: "top-center",
                                autoClose: 5000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        }
                 
                    })
                } catch(e) {
                    alert(e);
                } finally {
                    setLoading(false);
                }
            }
    return(
        <>
        <Header/>
        <Sidebar/>
        <div className="content-wrapper">
            <div className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                <div className="col-sm-6">
                    <h1 className="m-0">Manage Product</h1>
                </div>
                </div>
            </div>
            </div>
            <section className="content">
                <div className="container-fluid">
                
                    <div className="row">
                        <div className="card col-sm-12">
                            <div className="card-header">
                                <div className="col-sm-12">
                                    <button className="btn btn-primary btn-md" onClick={showAddModal}>
                                        <i className="fa fa-plus"> Add Products</i>
                                    </button>
                                </div>
                                    </div>
                                    <div className="card-body">
                                    <table className="table" id="tbl-product">
                                    <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                                
                                            </tr>
                                    </thead>
                                            <tbody>
                                        {isUpdate  &&
                                               <tr key={newdata.id} id={'tr'+newdata.id}>
                                            
                                               <td ><img src={"lte3/dist/img/" +newdata.prod_image} width="50px" height="50px"/> {newdata.prod_name}</td>
                                               <td>{newdata.quantity}</td>
                                               <td>{newdata.price}</td>
                                               <td><button className="btn btn-info btn-sm " onClick={()=> getProduct(newdata.id)}><i className="fas fa-edit"> Edit</i> </button> 
                                                    <button className="btn btn-danger btn-sm ml-1"  onClick={() => deleteProduct(newdata.id)}><i className="fas fa-times-circle"> Delete</i> </button>
                                                </td>
                                               </tr>
                                            
                                        }
                                        {isLoaded &&  products.map (row =>{
                                            return(
                                                <tr key={row.id} id={'tr'+row.id}>
                                            
                                                <td ><img src={"lte3/dist/img/" +row.prod_image} width="50px" height="50px"/> {row.prod_name}</td>
                                                <td>{row.quantity}</td>
                                                <td>{row.price}</td>
                                                <td><button className="btn btn-info btn-sm" onClick={()=> getProduct(row.id)}><i className="fas fa-edit"> Edit</i> </button> 
                                                    <button className="btn btn-danger btn-sm ml-1"  onClick={() => deleteProduct(row.id)}><i className="fas fa-times-circle"> Delete</i> </button>
                                                </td>
                                                </tr>

                                            ) }
                                            ) 
                                        }
                                         </tbody>
                                    <tfoot>

                                    </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
                <Modal size="md" show={prodModal} onHide={prodModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{modalTitle}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                       <div className="form-group ">
                           <label>Product Image</label>
                           <div className="input-group mb-3">
                                    <div className="custom-file">
                                        <input 
                                            type="file" 
                                            className="custom-file-input"
                                            accept="image/*"
                                            onChange={fileInput}
                                        />
                                        <label className="custom-file-label">{filename ? filename : 'Upload Image'}</label>
                                    </div>
                                </div>
                       </div>
                       <div className="form-group ">
                           <label>Product Description</label>
                           <input className="form-control " type="text" id="prod_name"
                            value ={prod_name}
                            onChange = {e=> setProdname(e.target.value)}
                           />
                       </div>
                       <div className="form-group col-sm-12">
                           <label>Product Quntity</label>
                           <input className="form-control " type="number" id="quantity"
                            value={quantity}
                            onChange = {e =>setQuantity(e.target.value)}
                           ></input>
                       </div>
                       <div className="form-group ">
                           <label>Product Price</label>
                           <input className="form-control " type="number" id="price"
                            value={price}
                            onChange = {e => setPrice(e.target.value)}

                           ></input>
                       </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={prodModalClose}>
                            Cancel
                        </Button>
                        {!btnUpdate ?( 
                            <Button variant="primary" disabled={loading}  onClick={addproducts}>
                            {loading ? 'Ssaving...' : 'Save'}
                        </Button>
                        ): (<Button variant="info" disabled={loading} onClick={updateProduct}  >
                        {loading ? 'Saving...' : 'Update'}
                    </Button>)}
                       <input type="hidden" id="prod_id" value ={id} />
                    </Modal.Footer>
                </Modal> 
                
            </div>
        </>
    );
 
}
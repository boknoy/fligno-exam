import React, {useState,useEffect, Component } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Modal,FormControl } from 'react-bootstrap';
import { toast } from 'react-toastify';
import api from '../api';
import 'react-toastify/dist/ReactToastify.css'
import Header from './Header'
import Sidebar from './SideBar'


export default function Sales()
{
    //Loading
    const [loading,setLoading] = useState(false)
    const [isLoaded,setLoaded] = useState(false)
    // get
    const [sales,setSales] = useState([])
    const [total,setTotal] = useState(0)
    useEffect(() => {
        api.getSales().then (res =>{
            setSales(res.data.sales)
            setTotal(res.data.total)
            setLoaded(true)
        })
    },[])
   
      const handleSales = () =>
      {
          if(isLoaded && sales)
          {
            
            return(
                  sales.map(row =>{
                      return(
                        <tr key={row.id}>
                            <td>{row.invoice}</td>
                            <td>{row.get_user.name}</td>
                            <td>{row.get_user.email}</td>
                            <td>₱ {row.total.toLocaleString(undefined, {maximumFractionDigits:2})}</td>
                        </tr>)
                  })
            )
              
          }
      }

    return( <>
    <Header/>
    <Sidebar/>
    <div className="content-wrapper">
    <div className="content-header">
    <div className="container-fluid">
        <div className="row mb-2">
        <div className="col-sm-6">
            <h1 className="m-0">Sales</h1>
        </div>
        </div>
    </div>
    </div>
    <section className="content">
        <div className="container-fluid">
            <div className="card">
                <div className="card-body">
                    <table className="table table-stripe ">
                        <thead>
                            <tr>
                                <th>Invoice#</th>
                                <th>Customer</th>
                                <th>Email</th>
                                <th>Amount</th>
                            </tr>
                            

                            </thead>
                        <tbody>
                            {handleSales()}
                        </tbody>
                        <tfoot>
                            <th colSpan={3} className="text-right">Total :</th>
                            <th >₱ {total.toLocaleString(undefined, {maximumFractionDigits:2})}</th>
                        </tfoot>

                    </table>
                </div>
            </div>
            
        </div>
        </section>
       
           
        
    </div>
    </>
)
}
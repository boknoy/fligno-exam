import React, { useState, useEffect } from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter,
  Switch,
  Route,
  Router,

} from "react-router-dom";
import LandingPage from './components/Landing/Landing'
import Login from './components/auth/Login'
import SideBar from './components/Admin/SideBar'
import Header from "./components/Admin/Header";
import DashBoard from "./components/Admin/Dashboard";
import Product from './components/Admin/Product'
import {Header as LandHeader} from './components/Landing/Header'
import Cart from './components/Landing/Cart'
import Checkout from './components/Landing/Checkout'
import Invoice from "./components/Landing/Invoice";
import Sales from "./components/Admin/Sales";
import ProtectedRoute from "./PrivateRoute";
import api from './components/api'
const App = () => {
  const [isAdmin,setIsAdmin] =useState(false)
  const [isLoaded,setLoaded] = useState(false)
 
    useEffect(() => {
      if(!localStorage.getItem('userAuth'))
      {
        setLoaded(true)
      }else{
      api.getAuth().then(res => {
          if(res.data.role=='Admin')
          {
            setIsAdmin(true)
          }else
          {
            setIsAdmin(false)
          }
          setLoaded(true)
          if(res.data.message)
          {
            alert(res.message)
          }
      });
    }
  }, []);
  
  
    console.log(isLoaded)
  
    if(isLoaded)
    {
      return (
        <BrowserRouter>
          <>
              <Switch>
                <Route exact path="/" component={LandingPage} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/cart">
            
                <Cart/>
    
                </Route>
                 
            
                <ProtectedRoute exact path="/home" component={DashBoard} isAdmin={isAdmin}/>
               
                 
                 
                  <ProtectedRoute exact path ="/products" isAdmin={isAdmin} component={Product}/>
                 
          
                  <ProtectedRoute exact path ="/sales" isAdmin={isAdmin} component={Sales}/>
                  <Route exact path="/checkout" component= {Checkout} />
                  <Route exact path="/invoice" component= {Invoice} />
                 
                <Route render={() => <h1 className="col-sm-12 text-center">404: page not found</h1>} />
               
              </Switch>
          </>
        </BrowserRouter>
      ) 
    }else{
      return(<h1>Fecthing...</h1>)
    }
 
}

ReactDOM.render(
  <App />,
  document.getElementById('index')
)
import React from 'react'
import {Route, Redirect} from 'react-router-dom'

function ProtectedRoute({isAdmin:isAdmin , component:Component,...rest})
{
    return<Route {...rest } render={(props)=>{

        if(isAdmin)
        {
            return <Component/>
        }else
        {
            return <h1 className="col-sm-12 text-center"> 404 Page Not Found</h1>
        }
    }}/>
}
export default ProtectedRoute
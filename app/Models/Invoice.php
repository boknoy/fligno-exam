<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    public function getUser() {
        return $this->hasOne(User::class, 'id', 'cus_id');
    }
}

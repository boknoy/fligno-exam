<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    public function getProd() {
        return $this->hasMany(Product::class, 'id', 'prod_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;
    public function getProd() {
        return $this->hasMany(Product::class, 'id', 'prod_id');
    }
    public function getUser() {
        return $this->hasOne(User::class, 'id', 'cus_id');
    }
}

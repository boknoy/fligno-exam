<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\CustomerDetails;
use App\Models\User;
use App\Models\Invoice;
class CustomerController extends Controller
{
    public function getAllCustomer()
    {
        $cus = User::where('role','Customer')->with('getDetails')->get();
        $cus_det = CustomerDetails::all();
        $sales = Invoice::sum('total');
        return response()->json([

            'customer' => $cus,
            'details' => $cus_det,
            'customerCount' => count($cus),
            'sales' => $sales
        ]);
    }
    public function register(Request $request) {
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|'
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'role' => 'Customer',
            'password' => bcrypt($fields['password'])
        ]);
        $customer = new CustomerDetails;
        $customer->address = $request->address;
        $customer->contact = $request->contact;
        $customer->cus_id = $user->id;
        $customer->save();
        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'customer' => $user,
            'details' => $customer,
            'token' => $token,
            'success' => 'Hi!'.$user->name
        ];

        return response($response, 201);
    }

    public function login(Request $request) {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response()->json(['failed'=>'Email and Password is invalid']);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;
        $det = CustomerDetails::where('cus_id',$user->id)->first();
        $response = [
            'user' => $user,
            'token' => $token,
            'det' => $det,
            'success'=>'Welcomeback '.$user->name
        ];

        return response()->json($response);
    }

    public function logout(Request $request) {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Logged out'
        ];
    }
    function getAuthDetails($id) {
        $view = User::all()->find($id);
        return response()->json($view);
    }
}

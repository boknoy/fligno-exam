<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\User;
use App\Models\CustomerDetails;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Order;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
       $f1 = Cart::where('cus_id',$r->cus_id)->where('prod_id',$r->prod_id)->count();
    if($f1 > 0)
    {
        return response()->json([
            'message' => 'Already in cart'
        ]);
    }
    $prod = Product::find($r->prod_id);
       $cart = new Cart;
       $cart->cus_id = $r->cus_id;
       $cart->prod_id = $r->prod_id;
       $cart->quantity = $r->quantity;
       $cart->total = $r->quantity * $prod->price;
       $cart->save();

       return response([
           'cart' => $cart,
           'success' => 'Success store in cart',
       ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cart = Cart::find($id);
        return response()->json($cart);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cart = Cart::find($id);
        return response()->json($cart);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $cart = Cart::find($id);
        $cart->quantity = $r->quantity;
        $cart->save();
        return response()->json($cart);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::find($id);
        $cart->delete();
        return response()->json([
            'message' => 'Deleted successfully',
        ]);
    }

    public function getAllCart($id)
    {
        $cart = Cart::with('getProd')->where('cus_id',$id)->get();
        $user = User::find($id);
        $det = CustomerDetails::where('cus_id',$id)->first();
        $total =Cart::where('cus_id',$id)->sum('total');
        $order = Order::with('getProd')->get();
        return response()->json([
            'customer' => $user,
            'det' => $det,
            'cart' => $cart,
            'success' => 'success',
            'total' => $total,
            'order' => $order
        ]);
    }
   
}

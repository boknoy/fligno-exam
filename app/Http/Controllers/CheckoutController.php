<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Sales;
use App\Models\Order;
use App\Models\User;
use App\Models\CustomerDetails;
use App\Models\Payment;
use Stripe;
use Carbon;
use App\Models\Invoice;
class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $inv =session('inv');
        $ordered = Sales::with('getProd')->where('invoice',$inv)->get();
        if(!isset($inv))
        {
            $inv = [];
        }
        $total = Sales::with('getProd')->where('invoice',$inv)->sum('total');
        return response()->json(['invoice'=>$inv,'ordered' =>$ordered,'total'=>$total]);
    }

    public function getOrder()
    {
        $order = Order::where('cus_id')->get();
        return response()->json($order);
    }

    public function ClearOrder($id)
    {
        $order  = Order::where('cus_id',$id)->delete();
        return response()->json([
            'message' => 'Order is empty'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $fields = $r->validate([
            'card_name' => 'required|string',
            'card_number' => 'required',
            'cvc' => 'required',
            'exp_month' => 'required',
            'exp_year' => 'required',

        ]);

       $order = Order::where('cus_id',$r->cus_id)->get();
      
       $inv = 'INV'.str_replace(str_split('\\/:*?"<>|-" "'), '',Carbon\Carbon::now());
       $unique = new Invoice;
       $unique->cus_id = $r->cus_id;
       $unique->invoice = $inv;
       $unique->total = $r->amount;
       $unique->save();
        foreach($order as $o)
        {
            $sales = new Sales;
            $sales -> prod_id = $o->prod_id;
            $sales -> cus_id = $r->cus_id;
            $sales -> invoice =  $inv;
            $sales -> quantity = $o->quantity;
            $sales -> total = $o->total;
            $sales->save();
        }
       $stripe = Stripe::make(env('STRIPE_KEY'));
        $cus = User::find($r->cus_id);
        $cus_det = CustomerDetails::where('cus_id',$cus->id)->first();
       try{
           $token = $stripe->tokens()->create([

            'card' => [
                'number' => $r->card_number,
                'exp_month' => $r->exp_month,
                'exp_year'  => $r->exp_year,
                'cvc' => $r->cvc
            ]
            ]);
            if(!isset($token['id']))
            {
                return response()->json([
                    'error' => 'Stripe key not generated'
                ]);
            }
            $stripe_cus = $stripe->customers()->create([
                'name' => $cus->name,
                'email' => $cus->email,
                'phone' => $cus_det ->contact,
                'source' => $token['id']

            ]);
            $charges = $stripe->charges()->create([
                'customer' =>$stripe_cus['id'],
                'currency' =>'PHP',
                'amount' => $r->amount,
                'description' => 'Payment for invoice no '.$inv
            ]);
            
       }catch(Exemption $e)
       {
           return response()->json([
               'message' => $e
           ]);
       }
       //$cart  = Cart::where('cus_id',$r->cus_id)->delete();
       //$payment = new Payment;
      // $payment -> cus_id = $r->cus_id;
       //$payment -> invoice =$inv;
      
       $invoice  = Sales::with('getProd')->with('getUser')->where('invoice',$inv)->get();
       foreach($order as $val)
       {
           $cart = Cart::where('id',$val->cart_id)->get();
           if(count($cart) > 0)
           {
               Cart::find($val->cart_id)->delete();
           }
       }
       $order = Order::truncate();
       session(['inv'=>$inv]);
       return response()->json([
           'invoice' => $invoice,
        'success' => 'Payment successfull'
    ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Cart;
use App\Models\CustomerDetails;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
       
        $user = User::find('cus_id');
        $qty = $r->quantity;
        $qty =explode(",", $qty);
        $cart = $r->cart_id;
        $cart=  explode(",", $cart);
        $order=[];
        foreach ($cart as $key => $value)
        {
            $c = Cart::find($value);
            $prod = Product::find($c->prod_id);
            $order = new Order;
            $order ->cart_id = $c->id;
            $order->cus_id = $r->cus_id;
            $order->prod_id = $c->prod_id;
            $order->quantity = intval($qty[$key]);
            $order->total =  intval($qty[$key]) * $prod->price;
           $order->save();
            
        }
        $order = Order::all();
        
        return response()->json(['order' => $order,'success' => 'order successfull' ]);
    }
    public function placeOrder($id,$qty,$cart_id,$cout)
    {
        for($i=0;$i<intval($cout);$i++)
        {
           /* $c = Cart::find($cart_id[$i]);
            //$prod = Product::find($c->prod_id);
            $order = new Order;
            $order ->cart_id = $c->id;
            $order->cus_id = $r->cus_id;
            $order->prod_id = $c->prod_id;
            $order->quantity = intval($qty[$i]);
            $order->total =  intval($qty[$i]) * $prod->price;
          // $order->save();*/
          //return $cout;
        }
        $a = explode(",", $cart_id);
        return response()->json(getType($a));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cart = Cart::with('getProd')->where('cus_id',$id)->get();
        $user = User::find($id);
        $det = CustomerDetails::where('cus_id',$id)->first();
        $total =Cart::where('cus_id',$id)->sum('total');
        $order = Order::with('getProd')->get();
        return response()->json([
            'customer' => $user,
            'det' => $det,
            'cart' => $cart,
            'success' => 'success',
            'total' => $total,
            'order' => $order
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return response()->json($product);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'prod_name' => 'required',
            'quantity' => 'required',
            'price' => 'required'
        ]);

    $product= Product::create($request->all());
     return response()->json([
         'product' => $product,
         'success' => 'Added successfully'
     ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prod = Product::find($id);
        return response()->json($prod);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProd(Request $r)
    {
        $product = Product::find($r->id);
        $product->prod_image = $r->prod_image;
        $product->prod_name= $r->prod_name;
        $product->price = $r->price;
        $product->quantity = $r->quantity;
        $product->save();
        return response()->json([
            'product' => $product,
            'success' => 'Updated Successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return response()->json([
            'success' => 'Deleted Successfully'
        ]);
    }

     /**
     * Search for a name
     *
     * @param  str  $name
     * @return \Illuminate\Http\Response
     */
    public function search($name)
    {
        return Product::where('prod_name', 'like', '%'.$name.'%')->get();
    }
}
